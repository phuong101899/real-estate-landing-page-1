import {Html, Head, Main, NextScript} from 'next/document'
import Script from 'next/script'

export default function Document() {
    return (
        <Html>
            <Head>
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700|" rel="stylesheet"
                      type="text/css"/>
                <link href="/fonts/font-awesome.min.css" rel="stylesheet" type="text/css"/>
            </Head>
            <body>
            <Main/>
            <NextScript/>
            <Script src="/js/jquery-1.11.1.min.js" />
            <Script src="/js/plugins.js" />
            <Script src="/js/app.js" />
            </body>
        </Html>
    )
}
