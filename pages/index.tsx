import type { NextPage } from 'next';

import houseImage from 'public/images/house.jpg';


const Home: NextPage = () => {
  return (
      <div className="site-content">
        <header
            className="site-header"
            style={{
              backgroundImage: `url(${houseImage.src}`
            }}
        >
          <div className="container">
            <a className="branding">
              <img src="/images/logo.png" alt="Apartments" />
                <h1 className="site-title">Apartments</h1>
                <small className="site-description">Live better</small>
            </a>
          </div>
          <div className="banner">
            <div className="banner-content">
              <div className="container">
                <div className="cta">
                  <a>
                    <span className="arrow-button"><i className="fa fa-angle-right"></i></span>
                    <h2>Neque porro quisquam est dolrem ipsum</h2>
                    <small>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore pariatur</small>
                  </a>
                </div>

                <div className="subscribe-form">
                  <form action="#">
                    <small className="form-subtitle">Start to live in Your</small>
                    <h2 className="form-title">New Apartment</h2>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nulla, dicta. Molestiae voluptatem
                      laudantium tempora nemo vitae</p>

                    <div className="control">
                      <input type="text" placeholder="Your name..." />
                        <i className="fa fa-user"></i>
                    </div>
                    <div className="control">
                      <input type="text" placeholder="Email address..."/>
                        <i className="fa fa-envelope"></i>
                    </div>
                    <div className="control">
                      <input type="text" placeholder="Phone number..." />
                        <i className="fa fa-phone"></i>
                    </div>
                    <input type="submit" value="Subscribe now" />
                  </form>
                </div>
              </div>
            </div>
          </div>
        </header>

        <main className="main-content">
          <div className="fullwidth-block">
            <div className="container">
              <h2 className="section-title">What is so great in our apartments?</h2>
              <div className="row">
                <div className="col-md-5">
                  <ul className="arrow-list">
                    <li>Nemo enim ipsam voluptatem voluptas</li>
                    <li>Quisquam dolorem ipsum quia dolor sit</li>
                    <li>Consectetur adipisci velit sed quia</li>
                    <li>Numquam eius modi tempora incidunt</li>
                    <li>Dolor in reprehenderit in voluptate velit</li>
                    <li>in reprehenderit in voluptate</li>
                  </ul>
                </div>
                <div className="col-md-7">
                  <div className="feature-slider">
                    <ul className="slides">
                      <li>
                        <figure>
                          <img src="/images/feature-1.jpg" alt="Feature 1"/>
                            <figcaption>
                              <h3 className="feature-title">Neque porro quisquam est dolorem ipsum</h3>
                              <small className="feature-desc">Velit esse cillum dolore pariatur</small>
                            </figcaption>
                        </figure>
                      </li>
                      <li>
                        <figure>
                          <img src="/images/feature-2.jpg" alt="Feature 2" />
                            <figcaption>
                              <h3 className="feature-title">Dolor in reprehenderit in voluptate velit</h3>
                              <small className="feature-desc">Reprehenderit in voluptate velit</small>
                            </figcaption>
                        </figure>
                      </li>
                      <li>
                        <figure>
                          <img src="/images/feature-3.jpg" alt="Feature 3"/>
                            <figcaption>
                              <h3 className="feature-title">Quisquam dolorem ipsum quia dolor sit</h3>
                              <small className="feature-desc">Quia dolor ipsum quia dolor sit</small>
                            </figcaption>
                        </figure>
                      </li>
                    </ul>

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="fullwidth-block" data-bg-color="#f0f0f0">
            <div className="container">
              <div className="row">
                <div className="col-md-3 col-sm-6">
                  <div className="feature">
                    <div className="feature-icon"><img src="/images/icon-location.png" alt=""/></div>
                    <h3 className="feature-title">Perfect location</h3>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
                      magni.</p>
                  </div>
                </div>
                <div className="col-md-3 col-sm-6">
                  <div className="feature">
                    <div className="feature-icon"><img src="/images/icon-golf.png" alt=""/></div>
                    <h3 className="feature-title">Golf course</h3>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
                      magni.</p>
                  </div>
                </div>
                <div className="col-md-3 col-sm-6">
                  <div className="feature">
                    <div className="feature-icon"><img src="/images/icon-valet.png" alt=""/></div>
                    <h3 className="feature-title">Private valet</h3>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
                      magni.</p>
                  </div>
                </div>
                <div className="col-md-3 col-sm-6">
                  <div className="feature">
                    <div className="feature-icon"><img src="/images/icon-phone.png" alt="" /></div>
                    <h3 className="feature-title">24/h infoline</h3>
                    <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
                      magni.</p>
                  </div>
                </div>
              </div>

              <div className="post-list">
                <article className="post">
                  <figure className="feature-image" data-bg-image="images/figure-1.jpg"></figure>
                  <div className="post-detail">
                    <h2 className="entry-title">Lorem porro quisquam dolorem</h2>
                    <p>Omnis iste natus error sit voluptatem doloremque laudantium totam rem aperiam, eaque ipsa quae ab
                      illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo nemo enim ipsam
                      voluptatem quia.</p>
                    <a href="#" className="button">See more</a>
                  </div>
                </article>
                <article className="post">
                  <figure className="feature-image" data-bg-image="images/figure-1.jpg"></figure>
                  <div className="post-detail">
                    <h2 className="entry-title">Lorem porro quisquam dolorem</h2>
                    <p>Omnis iste natus error sit voluptatem doloremque laudantium totam rem aperiam, eaque ipsa quae ab
                      illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo nemo enim ipsam
                      voluptatem quia.</p>
                    <a className="button">See more</a>
                  </div>
                </article>
                <article className="post">
                  <figure className="feature-image">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/qnUjbjO-e0E"
                            title="YouTube video player" frameBorder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowFullScreen></iframe>
                  </figure>
                  <div className="post-detail">
                    <h2 className="entry-title">Lorem porro quisquam dolorem</h2>
                    <p>Omnis iste natus error sit voluptatem doloremque laudantium totam rem aperiam, eaque ipsa quae ab
                      illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo nemo enim ipsam
                      voluptatem quia.</p>
                    <a className="button">See more</a>
                  </div>
                </article>
              </div>


            </div>
          </div>
          <div className="fullwidth-block">
            <div className="container">
              <h2 className="section-title">Meet our partners</h2>
              <div className="partners">
                <a ><img src="/images/partner-1.png" alt="" /></a>
                <a ><img src="/images/partner-2.png" alt="" /></a>
                <a ><img src="/images/partner-3.png" alt="" /></a>
                <a ><img src="/images/partner-4.png" alt="" /></a>
                <a ><img src="/images/partner-5.png" alt="" /></a>
              </div>
            </div>
          </div>
        </main>

        <footer className="site-footer">
          <div className="container">
            <p>Copyright 2014 Company name. Designed by Themezy. All rights reserved</p>
            <div className="social-links">
              <a href="#" className="facebook"><i className="fa fa-facebook"></i></a>
              <a href="#" className="twitter"><i className="fa fa-twitter"></i></a>
              <a href="#" className="google-plus"><i className="fa fa-google-plus"></i></a>
              <a href="#" className="pinterest"><i className="fa fa-pinterest"></i></a>
            </div>
          </div>
        </footer>
      </div>
  );
}

export default Home
