import 'styles/sass/style.scss';
import type { AppProps } from 'next/app'
import Head from 'next/head';

function MyApp({ Component, pageProps }: AppProps) {
  return (
      <>
        <Head>
          <title>Compass Starter by Ariona, Rian</title>
          <meta charSet="UTF-8"/>
          <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
          <meta name="viewport" content="width=device-width, initial-scale=1.0,maximum-scale=1"/>
        </Head>
          <Component {...pageProps} />
      </>
  )
}

export default MyApp
